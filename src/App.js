import React, { Component } from "react";
import Grid from "./components/SquareGrid/Grid";
import LevelGame from "./components/LevelGame/LevelGame";
import RemainShoots from "./components/RemainShoots/RemainShoots";
import GameResultModal from "./components/GameResultModal/GameResultModal";
import ListOfGames from "./components/ListOfGames/ListOfGames";
import BattleShip from "./components/BattleShip/BattleShip";
import ControlPanel from "./components/ControlPanel/ControlPanel";
import Board from "./components/Board/Board";
import {
  occupySquares,
  isOverlaping,
  isItOutsideTheGrid,
  formatDate
} from "./components/Utils/Utils";
import "./App.css";

class App extends Component {
  state = {
    squares: Array(100).fill(null),
    numberOfTurns: 0,
    isTheGameStarted: false,
    isGameOver: false,
    numberOfAttempts: 0,
    enemyShips: (this.props.ships && this.props.ships.length) || 10,
    startDate: formatDate(new Date()),
    games: JSON.parse(localStorage.getItem("games")) || [{}]
  };

  get isGameEnd() {
    return (
      this.state.enemyShips === 0 ||
      (this.state.numberOfTurns === this.state.numberOfAttempts &&
        this.state.numberOfAttempts !== 0)
    );
  }

  get isTheStartButtonDisabled() {
    return !this.state.numberOfTurns || this.state.isTheGameStarted;
  }
  positionShipOnBoard = () => {
    let location;
    let alignment;
    let nextBoard = this.state.squares;
    this.props.ships.forEach(ship => {
      do {
        location = Math.floor(Math.random() * 100);
        alignment =
          Math.floor(Math.random() * 2) === 1 ? "horizontal" : "vertical";
      } while (
        isOverlaping(location, ship.spaceLong, nextBoard, alignment) ||
        isItOutsideTheGrid(location, ship.spaceLong, alignment)
      );

      nextBoard = occupySquares(
        location,
        ship.spaceLong,
        nextBoard,
        ship.id,
        alignment
      );
    });

    this.setState({ squares: nextBoard });
  };

  shoot = (squareIndex, firingResult) => {
    if (firingResult === "REPETED_SHOT") {
      return;
    }

    if (firingResult === "SINK" && this.state.enemyShips - 1 === 0) {
      this.setState({ enemyShips: 0 });
      return;
    }

    if (firingResult === "SINK") {
      this.setState({ enemyShips: this.state.enemyShips - 1 }, () =>
        window.alert(
          `You Sink a ship, there are ${this.state.enemyShips} remaining`
        )
      );
    }

    let newSquares = this.state.squares;
    newSquares[squareIndex] = firingResult === "SINK" ? "HIT" : firingResult;

    this.setState({
      squares: newSquares,
      numberOfAttempts: this.state.numberOfAttempts + 1
    });
  };
  startGame = () => {
    this.setState({
      isTheGameStarted: true,
      startDate: formatDate(new Date())
    });
    this.positionShipOnBoard(this.props.ships);
  };

  onSelectLevelGame = numberOfTurns => {
    this.setState({
      numberOfTurns: Number(numberOfTurns)
    });
  };

  stopGame = () => {
    const game = {
      startDate: this.state.startDate,
      endDate: formatDate(new Date()),
      attempts: this.state.numberOfAttempts
    };

    this.setState(
      {
        squares: Array(100).fill(null),
        numberOfTurns: 0,
        isTheGameStarted: false,
        isGameOver: false,
        numberOfAttempts: 0,
        enemyShips: 10,
        games: [...this.state.games, game]
      },
      () => localStorage.setItem("games", JSON.stringify(this.state.games))
    );
  };

  render() {
    return (
      <BattleShip>
        <ControlPanel>
          <LevelGame
            onSelectLevelGame={this.onSelectLevelGame}
            disabled={this.state.isTheGameStarted}
            numberOfTurns={this.state.numberOfTurns}
          />
          <br />
          <button
            disabled={this.isTheStartButtonDisabled}
            onClick={() => this.startGame()}
            className={"button"}
          >
            Start to Play
          </button>
          <RemainShoots
            number={this.state.numberOfTurns - this.state.numberOfAttempts}
          />
        </ControlPanel>
        <Board>
          <Grid
            squares={this.state.squares}
            squareOnClick={(index, firingResult) =>
              this.shoot(index, firingResult)
            }
            disabled={!this.state.isTheGameStarted}
          />
          <ListOfGames games={this.state.games} />
        </Board>
        <GameResultModal
          isOpen={this.isGameEnd ? true : false}
          message={this.state.enemyShips === 0 ? "You Win!!" : "Game Over"}
          onOkButtonClick={() => {
            this.stopGame();
          }}
        />
      </BattleShip>
    );
  }
}

export default App;
