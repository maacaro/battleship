import React from "react";
import PropTypes from "prop-types";

const GameResultModal = props => {
  if (props.isOpen === false) {
    return null;
  }
  return (
    <div className="modalDialog">
      <div className="message">
        <h2>{props.message}</h2>
        <a href="#" onClick={() => props.onOkButtonClick()}>
          Try Again
        </a>:
      </div>
    </div>
  );
};

export default GameResultModal;

GameResultModal.proptypes = {
  message: PropTypes.string,
  onOkButtonClick: PropTypes.func
};
