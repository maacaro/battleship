import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import GameResultModal from "./GameResultModal";

describe("the render", () => {
  describe("when is visible", () => {
    it("should render the text `Custom Text`", () => {
      const modal = shallow(
        <GameResultModal isOpen={true} message={"Custom Text"} />
      );

      expect(modal.find("h2").text()).toEqual("Custom Text");
    });
  });

  describe("when is NOT Visible", () => {
    it("should NOT render", () => {
      const modal = shallow(
        <GameResultModal isOpen={false} message={"Custon Text"} />
      );

      expect(modal.children().length).toEqual(0);
    });
  });
});

describe("the interaction", () => {
  describe("when the `OK` button is clicked", () => {
    it("should call the prop function `onOkButtonClick`", () => {
      const onOkButtonClickMock = jest.fn();
      const modal = shallow(
        <GameResultModal
          isOpen={true}
          message={"Custon Text"}
          onOkButtonClick={onOkButtonClickMock}
        />
      );

      modal
        .find("a")
        .props()
        .onClick();

      expect(onOkButtonClickMock).toHaveBeenCalledTimes(1);
    });
  });
});
