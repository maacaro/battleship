import React from "react";
import { shallow } from "enzyme";
import LevelGame from "./LevelGame";

describe("the Render", () => {
  it("should render a select element", () => {
    const LevelGameWrapper = shallow(<LevelGame />);

    expect(LevelGameWrapper.find("div").find("select")).toHaveLength(1);
  });

  it("should render a select element with the number of turns allowed", () => {
    const LevelGameWrapper = shallow(<LevelGame />);
    const selectLevelGameOptions = [
      <option value={0} defaultValue>
        Game Level
      </option>,
      <option value={Infinity}>Easy</option>,
      <option value={100}>Mediun</option>,
      <option value={50}>Hard</option>
    ];

    expect(
      LevelGameWrapper.find("div")
        .find("select")
        .contains(selectLevelGameOptions)
    ).toEqual(true);
  });

  it("should render the select element with the disabled prop as true", () => {
    const LevelGameWrapper = shallow(<LevelGame disabled={true} />);

    expect(LevelGameWrapper.find("select").props().disabled).toEqual(true);
  });

  it("should render the select element with the disabled prop as false", () => {
    const LevelGameWrapper = shallow(<LevelGame disabled={false} />);

    expect(LevelGameWrapper.find("select").props().disabled).toEqual(false);
  });

  it("should render the number of turns", () => {
    const appWrapper = shallow(<LevelGame numberOfTurns={50} />);

    expect(appWrapper.find(".number").text()).toEqual("50");
  });
});

describe("the interaction", () => {
  it("should call the onSelectLevelGame function prop", () => {
    const onSelectLevelGameMock = jest.fn();
    const mockedEvent = { target: { value: 2 } };
    const LevelGameWrapper = shallow(
      <LevelGame onSelectLevelGame={onSelectLevelGameMock} />
    );

    LevelGameWrapper.find("select")
      .props()
      .onChange(mockedEvent);

    expect(onSelectLevelGameMock).toHaveBeenCalledTimes(1);
  });
});
