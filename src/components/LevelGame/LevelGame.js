import React from "react";
import PropTypes from "prop-types";

const LevelGame = props => (
  <div>
    <p>Please Select a Game Level</p>
    <select
      onChange={e => props.onSelectLevelGame(e.target.value)}
      value={props.numberOfTurns}
      disabled={props.disabled}
    >
      <option value={0} defaultValue>
        Game Level
      </option>
      <option value={Infinity}>Easy</option>
      <option value={100}>Mediun</option>
      <option value={50}>Hard</option>
    </select>
    <p>Your number of turns will be: </p>
    <div className="number">{props.numberOfTurns}</div>
  </div>
);

export default LevelGame;

LevelGame.proptypes = {
  numberOfTurns: PropTypes.string,
  onSelectLevelGame: PropTypes.func,
  disabled: PropTypes.bool
};
