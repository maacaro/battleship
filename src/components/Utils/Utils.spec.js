import React from "react";
import {
  getFiringResult,
  occupySquares,
  isOverlaping,
  isItOutsideTheGrid,
  formatDate
} from "./Utils";

describe("isItOutsideTheGrid", () => {
  describe("horizontal", () => {
    it("should return `false` when the ship is Not Outside of the Grid", () => {
      const initialPointOfLocation = 10;
      const spaceLongOfTheShip = 5;
      const alignment = "horizontal";

      expect(
        isItOutsideTheGrid(
          initialPointOfLocation,
          spaceLongOfTheShip,
          alignment
        )
      ).toEqual(false);
    });

    describe("when the ship is Outside of the Grid", () => {
      it("should return `true` when the initial Point of the ship`s location is at the end on the row", () => {
        const initialPointOfLocation = 9;
        const spaceLongOfTheShip = 2;
        const alignment = "horizontal";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is NOT at the end on the row", () => {
        const initialPointOfLocation = 17;
        const spaceLongOfTheShip = 4;
        const alignment = "horizontal";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is at the end on the column", () => {
        const initialPointOfLocation = 97;
        const spaceLongOfTheShip = 5;
        const alignment = "horizontal";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is at the end on the column and row", () => {
        const initialPointOfLocation = 99;
        const spaceLongOfTheShip = 2;
        const alignment = "horizontal";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });
    });
  });

  describe("vertical", () => {
    it("should return `false` when the ship is Not Outside of the Grid", () => {
      const initialPointOfLocation = 58;
      const spaceLongOfTheShip = 5;
      const alignment = "vertical";

      expect(
        isItOutsideTheGrid(
          initialPointOfLocation,
          spaceLongOfTheShip,
          alignment
        )
      ).toEqual(false);
    });

    describe("when the ship is Outside of the Grid", () => {
      it("should return `true` when the initial Point of the ship`s location is at the end on the row", () => {
        const initialPointOfLocation = 89;
        const spaceLongOfTheShip = 3;
        const alignment = "vertical";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is NOT at the end on the row", () => {
        const initialPointOfLocation = 77;
        const spaceLongOfTheShip = 4;
        const alignment = "vertical";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is at the end on the column", () => {
        const initialPointOfLocation = 97;
        const spaceLongOfTheShip = 2;
        const alignment = "vertical";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });

      it("should return `true` when the initial Point of the ship`s location is at the end on the column and row", () => {
        const initialPointOfLocation = 99;
        const spaceLongOfTheShip = 2;
        const alignment = "vertical";

        expect(
          isItOutsideTheGrid(
            initialPointOfLocation,
            spaceLongOfTheShip,
            alignment
          )
        ).toEqual(true);
      });
    });
  });
});

describe("isOverlaping", () => {
  describe("horizontal", () => {
    it("Should return `false` when the ship will NOT overlap another one", () => {
      const initialPointOfLocation = 10;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 5, 6);
      const spaceLongOfTheShip = 5;
      const alignment = "horizontal";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(false);
    });

    it("Should return true when the ship will overlap another one from the initialPointOfLocation", () => {
      const initialPointOfLocation = 4;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 4, 6);
      const spaceLongOfTheShip = 5;
      const alignment = "horizontal";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(true);
    });

    it("Should return true when the ship will overlap another one, before the initialPointOfLocation", () => {
      const initialPointOfLocation = 4;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 5, 6);
      const spaceLongOfTheShip = 5;
      const alignment = "horizontal";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(true);
    });
  });

  describe("vertical", () => {
    it("Should return `false` when the ship will NOT overlap another one", () => {
      const initialPointOfLocation = 10;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 12, 18);
      const spaceLongOfTheShip = 4;
      const alignment = "vertical";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(false);
    });

    it("Should return `true` when the ship will overlap another one in the initialPointOfLocation", () => {
      const initialPointOfLocation = 10;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 10, 11);
      const spaceLongOfTheShip = 4;
      const alignment = "vertical";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(true);
    });

    it("Should return `true` when the ship will overlap another one in the finalPointOfLocation", () => {
      const initialPointOfLocation = 10;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 40, 41);
      const spaceLongOfTheShip = 4;
      const alignment = "vertical";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(true);
    });

    it("Should return `true` when the ship will overlap another one in any square beteewn the initial and the final PointOfLocation", () => {
      const initialPointOfLocation = 10;
      let squares = Array(100).fill(null);
      squares.fill("occupied", 20, 21);
      const spaceLongOfTheShip = 4;
      const alignment = "vertical";

      expect(
        isOverlaping(
          initialPointOfLocation,
          spaceLongOfTheShip,
          squares,
          alignment
        )
      ).toEqual(true);
    });
  });
});

describe("occupySquares", () => {
  describe("horizontal", () => {
    it("should occupy as many square as the size of the ship", () => {
      const currentGrid = Array(100).fill(null);
      const spaceLongOfTheShip = 2;

      const nextGrid = occupySquares(
        10,
        spaceLongOfTheShip,
        currentGrid,
        "occupied",
        "horizontal"
      );

      expect(nextGrid[10]).toEqual("occupied");
      expect(nextGrid[11]).toEqual("occupied");
      expect(nextGrid.filter(square => square == "occupied").length).toEqual(2);
    });
  });
  describe("Vertical", () => {
    it("should occupy as many square as the size of the ship", () => {
      const currentGrid = Array(100).fill(null);
      const spaceLongOfTheShip = 2;

      const nextGrid = occupySquares(
        0,
        spaceLongOfTheShip,
        currentGrid,
        "occupied",
        "vertical"
      );

      expect(nextGrid[0]).toEqual("occupied");
      expect(nextGrid[10]).toEqual("occupied");
      expect(nextGrid.filter(square => square === "occupied").length).toEqual(
        spaceLongOfTheShip
      );
    });
  });
});

describe("getFiringResult", () => {
  it("should return the miss when the shoot is on an empty square", () => {
    let grid = [null, null, "s2", "s2", null];
    const square = 4;
    const getFiringResultResult = getFiringResult(grid, square);

    expect(getFiringResultResult).toEqual("MISS");
  });

  it("should return the HIT when the shoot is on square with a ship in it", () => {
    let grid = [null, null, "s2", "s2", null];
    const square = 3;
    const getFiringResultResult = getFiringResult(grid, square);

    expect(getFiringResultResult).toEqual("HIT");
  });

  it("should return SINK when the shoot is the last square accupied by some class of ship", () => {
    let grid = ["MISS", null, "HIT", "s2", null];
    const square = 3;
    const getFiringResultResult = getFiringResult(grid, square);

    expect(getFiringResultResult).toEqual("SINK");
  });

  it("should return REPETED_SHOT when shoot is on a square already HIT", () => {
    let grid = ["MISS", null, "HIT", "s2", null];
    const square = 0;
    const getFiringResultResult = getFiringResult(grid, square);

    expect(getFiringResultResult).toEqual("REPETED_SHOT");
  });

  it("should return REPETED_SHOT when shoot is on a square already MISS", () => {
    let grid = ["MISS", null, "HIT", "s2", null];
    const square = 2;
    const getFiringResultResult = getFiringResult(grid, square);

    expect(getFiringResultResult).toEqual("REPETED_SHOT");
  });
});

describe("formatDate", () => {
  it("should renturn a string", () => {
    const formatedDate = formatDate(new Date());

    expect(formatedDate).toEqual(expect.any(String));
  });

  it("should return a date in the format MM-DD-YYYY hh:mm", () => {
    var date = new Date("Mon Feb 26 2018 19:36:13 GMT-0300 (-03)");

    const formatedDate = formatDate(date);

    expect(formatedDate).toEqual("02-26-2018 07:36");
  });
});
