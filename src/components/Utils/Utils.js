export function isItOutsideTheGrid(
  initialPointOfLocation,
  spaceLongOfTheShip,
  alignment
) {
  if (alignment === "horizontal") {
    if (
      initialPointOfLocation % 10 >
      (initialPointOfLocation + spaceLongOfTheShip) % 10
    ) {
      return true;
    }
  }
  if (alignment === "vertical") {
    if (
      initialPointOfLocation + (spaceLongOfTheShip - 1) * 10 >
      90 + initialPointOfLocation % 10
    ) {
      return true;
    }
  }

  return false;
}

export function occupySquares(
  selectedSquareIndex,
  spaceLongOfTheShip,
  currentGrid,
  occupiedBy,
  alignment
) {
  if (alignment === "horizontal") {
    return currentGrid.map((square, i) => {
      if (
        i >= selectedSquareIndex &&
        i < selectedSquareIndex + spaceLongOfTheShip
      ) {
        return occupiedBy;
      }
      return square;
    });
  }
  return currentGrid.map((square, i) => {
    if (
      selectedSquareIndex <= i &&
      selectedSquareIndex + 10 * (spaceLongOfTheShip - 1) >= i &&
      selectedSquareIndex % 10 === (i + 10 * (spaceLongOfTheShip - 1)) % 10
    ) {
      return occupiedBy;
    }
    return square;
  });
}

export function isOverlaping(
  initialPointOfLocation,
  spaceLongOfTheShip,
  squares,
  alignment
) {
  if (alignment === "horizontal") {
    if (
      squares
        .slice(
          initialPointOfLocation,
          initialPointOfLocation + spaceLongOfTheShip
        )
        .some(square => square !== null)
    ) {
      return true;
    }
  }
  if (alignment === "vertical") {
    for (let i = 0; i < spaceLongOfTheShip; i++) {
      if (squares[initialPointOfLocation + 10 * i] !== null) {
        return true;
      }
    }
  }
  return false;
}

export function getFiringResult(grid, square) {
  const squareOccupiedBy = grid[square];
  let nextGrid = [...grid];

  if (grid[square] === null) {
    return "MISS";
  }

  if (grid[square] === "MISS" || grid[square] === "HIT") {
    return "REPETED_SHOT";
  }

  nextGrid[square] = "HIT";
  if (nextGrid.includes(squareOccupiedBy)) {
    return "HIT";
  }

  return "SINK";
}

Number.prototype.padLeft = function(base, chr) {
  var len = String(base || 10).length - String(this).length + 1;
  return len > 0 ? new Array(len).join(chr || "0") + this : this;
};

export function formatDate(date) {
  const formatDate =
    [
      (date.getMonth() + 1).padLeft(),
      date.getDate().padLeft(),
      date.getFullYear()
    ].join("-") +
    " " +
    [(date.getHours() % 12 || 12).padLeft(), date.getMinutes().padLeft()].join(
      ":"
    );
  return formatDate;
}
