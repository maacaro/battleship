import React from "react";
import { shallow } from "enzyme";
import BattleShip from "./BattleShip";

describe("the render", () => {
  it("should contain a wrapper div with the class flex-container", () => {
    const BattleShipWrapper = shallow(<BattleShip />);

    expect(BattleShipWrapper.find("#wrapper").props().className).toEqual(
      "flex-container"
    );
  });
  it("should contain a header tag", () => {
    const BattleShipWrapper = shallow(<BattleShip />);

    expect(BattleShipWrapper.find("header")).toHaveLength(1);
  });

  it("should render its children", () => {
    const BattleShipWrapper = shallow(
      <BattleShip>
        <div />
        <div />
      </BattleShip>
    );
    expect(BattleShipWrapper.children()).toHaveLength(3);
  });
});
