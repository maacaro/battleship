import React from "react";

const BattleShip = props => {
  return (
    <div id={"wrapper"} className="flex-container">
      <header>
        <h1>Battle Ship / One Single Player</h1>
      </header>
      {props.children}
    </div>
  );
};
export default BattleShip;
