import React from "react";
import PropTypes from "prop-types";

const ListOfGames = props => (
  <div className={"centered"}>
    <table>
      <thead align="center">
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Attempts</th>
        </tr>
      </thead>
      <tbody>
        {props.games.map((game, index) => (
          <tr key={index}>
            <th>{game.startDate}</th>
            <th>{game.endDate}</th>
            <th>{game.attempts}</th>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

export default ListOfGames;
