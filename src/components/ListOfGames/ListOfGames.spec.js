import React from "react";
import { shallow } from "enzyme";
import ListOfGames from "./ListOfGames";

describe("the render", () => {
  it("should render a table", () => {
    const listOfGames = shallow(<ListOfGames games={[]} />);

    expect(listOfGames.find("table")).toHaveLength(1);
  });

  it("should have as many children as games", () => {
    const listOfGames = shallow(<ListOfGames games={[{}, {}]} />);

    expect(
      listOfGames
        .find("table")
        .find("tbody")
        .children()
    ).toHaveLength(2);
  });
});
