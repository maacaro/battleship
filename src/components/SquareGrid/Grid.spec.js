import React from "react";
import { shallow } from "enzyme";
import Grid from "./Grid";

describe("the Render", () => {
  it("should render 1 square", () => {
    const squares = Array(1).fill(null);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect(gridWraper.find("Square")).toHaveLength(1);
  });

  it("should render 100 square", () => {
    const squares = Array(100).fill(null);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect(gridWraper.find("Square")).toHaveLength(100);
  });

  it("should render a square with clasName prop set to square", () => {
    const squares = Array(100).fill(null);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect(gridWraper.find({ type: "square" })).toHaveLength(100);
  });

  it("should render a square with type prop set to `HIT`", () => {
    const squares = Array(100).fill(null);
    squares.fill("HIT", 40, 41);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect.assertions(2);
    expect(
      gridWraper
        .find("Square")
        .at(40)
        .props().type
    ).toEqual("HIT");
    expect(gridWraper.find({ type: "HIT" })).toHaveLength(1);
  });

  it("should render squares with type prop set to `MISS`", () => {
    const squares = Array(100).fill(null);
    squares.fill("MISS", 40, 41);
    squares.fill("MISS", 50, 51);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect.assertions(3);
    expect(
      gridWraper
        .find("Square")
        .at(40)
        .props().type
    ).toEqual("MISS");
    expect(
      gridWraper
        .find("Square")
        .at(50)
        .props().type
    ).toEqual("MISS");
    expect(gridWraper.find({ type: "MISS" })).toHaveLength(2);
  });

  it("should render squares with type prop set to `square`", () => {
    const squares = Array(100).fill(null);
    squares.fill("s1", 40, 41);
    squares.fill("s1", 50, 51);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect.assertions(2);
    expect(
      gridWraper
        .find("Square")
        .at(40)
        .props().type
    ).toEqual("square");
    expect(
      gridWraper
        .find("Square")
        .at(50)
        .props().type
    ).toEqual("square");
  });

  it("should render 10 `RowNumber`", () => {
    const squares = Array(100).fill(null);
    const gridWraper = shallow(<Grid squares={squares} />);

    expect(gridWraper.find("RowNumber")).toHaveLength(10);
  });
});

describe("the interaction", () => {
  it("should call the squareOnClick function at onClick with the square Index", () => {
    const squares = Array(100).fill(null);
    const squareOnClickMock = jest.fn();
    const gridWraper = shallow(
      <Grid squares={squares} squareOnClick={squareOnClickMock} />
    );

    gridWraper
      .find("Square")
      .at(40)
      .props()
      .squareOnClick();

    expect(squareOnClickMock).toHaveBeenCalledWith(40, expect.any(String));
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });

  it("should call the squareOnClick function at onClick with the string MISS", () => {
    const squares = Array(100).fill(null);
    const squareOnClickMock = jest.fn();
    const gridWraper = shallow(
      <Grid squares={squares} squareOnClick={squareOnClickMock} />
    );

    gridWraper
      .find("Square")
      .at(40)
      .props()
      .squareOnClick();

    expect(squareOnClickMock).toHaveBeenCalledWith(40, "MISS");
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });

  it("should call the squareOnClick function at onClick with the string HIT", () => {
    let squares = Array(100).fill(null);
    squares.fill("s4", 40, 42);
    const squareOnClickMock = jest.fn();
    const gridWraper = shallow(
      <Grid squares={squares} squareOnClick={squareOnClickMock} />
    );

    gridWraper
      .find("Square")
      .at(40)
      .props()
      .squareOnClick();

    expect(squareOnClickMock).toHaveBeenCalledWith(40, "HIT");
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });

  it("should call the squareOnClick function at onClick with the string SINK", () => {
    let squares = Array(100).fill(null);
    squares.fill("s4", 40, 41);
    const squareOnClickMock = jest.fn();
    const gridWraper = shallow(
      <Grid squares={squares} squareOnClick={squareOnClickMock} />
    );

    gridWraper
      .find("Square")
      .at(40)
      .props()
      .squareOnClick();

    expect(squareOnClickMock).toHaveBeenCalledWith(40, "SINK");
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });

  it("should call the squareOnClick function at onClick with the string `REPETED_SHOT`", () => {
    let squares = Array(100).fill(null);
    squares.fill("MISS", 40, 41);
    squares.fill("HIT", 50, 51);
    const squareOnClickMock = jest.fn();
    const gridWraper = shallow(
      <Grid squares={squares} squareOnClick={squareOnClickMock} />
    );

    gridWraper
      .find("Square")
      .at(40)
      .props()
      .squareOnClick();
    expect(squareOnClickMock).toHaveBeenCalledWith(40, "REPETED_SHOT");
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
    squareOnClickMock.mockClear();

    gridWraper
      .find("Square")
      .at(50)
      .props()
      .squareOnClick();
    expect(squareOnClickMock).toHaveBeenCalledWith(50, "REPETED_SHOT");
    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });
});
