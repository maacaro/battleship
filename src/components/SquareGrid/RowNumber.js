import React from "react";
import PropTypes from "prop-types";

const RowNumber = props => {
  return (
    <div id={"rowWrapper"}>
      <div id={"number"} className={"non-square"}>
        {Math.floor(props.number / 10) + 1}
      </div>
      {props.children}
    </div>
  );
};

export default RowNumber;
