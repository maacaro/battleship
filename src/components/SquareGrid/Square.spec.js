import React from "react";
import { shallow } from "enzyme";
import Square from "./Square";

describe("the render", () => {
  it("should render a div", () => {
    const squareWrapper = shallow(<Square />);

    expect(squareWrapper.find("div")).toHaveLength(1);
  });

  it("should render a div with the className set to its prop type", () => {
    const squareWrapper = shallow(<Square type={"unknow"} />);

    expect(squareWrapper.find("div").props().className).toEqual("unknow");
  });
});

describe("the interaction", () => {
  it("should call the squareOnClick funtion at OnClick", () => {
    const squareOnClickMock = jest.fn();
    const squareWrapper = shallow(<Square squareOnClick={squareOnClickMock} />);

    squareWrapper
      .find("div")
      .props()
      .onClick();

    expect(squareOnClickMock).toHaveBeenCalledTimes(1);
  });
});
