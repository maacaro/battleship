import React from "react";
import { shallow } from "enzyme";
import RowNumber from "./RowNumber";

describe("the render", () => {
  it("should render a Number equal to its row number props", () => {
    const rowNumberWrapper = shallow(<RowNumber number={9} />);

    expect(rowNumberWrapper.find("#number").text()).toEqual("1");
  });
  it("should render its children", () => {
    const rowNumberWrapper = shallow(
      <RowNumber number={9}>
        <div id={"children"}>children</div>
      </RowNumber>
    );

    expect(rowNumberWrapper.find("#children").text()).toEqual("children");
  });
});
