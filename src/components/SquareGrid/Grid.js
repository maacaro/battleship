import React from "react";
import Square from "./Square";
import { getFiringResult } from "../Utils/Utils";
import RowNumber from "./RowNumber";

const columns = [
  <div key={0} className="non-square" />,
  <div key={1} className="non-square">
    A
  </div>,
  <div key={2} className="non-square">
    B
  </div>,
  <div key={3} className="non-square">
    C
  </div>,
  <div key={4} className="non-square">
    D
  </div>,
  <div key={5} className="non-square">
    E
  </div>,
  <div key={6} className="non-square">
    F
  </div>,
  <div key={7} className="non-square">
    G
  </div>,
  <div key={8} className="non-square">
    H
  </div>,
  <div key={9} className="non-square">
    I
  </div>,
  <div key={10} className="non-square">
    j
  </div>
];

const getSquareType = square => {
  return square === "HIT" || square === "MISS" ? square : "square";
};

const Grid = props => {
  return (
    <div id="wrapper">
      {columns}
      {props.disabled
        ? props.squares.map(
            (square, index) =>
              index % 10 === 0 ? (
                <RowNumber key={index} number={index}>
                  <Square
                    key={index}
                    type={"square"}
                    squareOnClick={() => {}}
                  />
                </RowNumber>
              ) : (
                <Square key={index} type={"square"} squareOnClick={() => {}} />
              )
          )
        : props.squares.map(
            (square, index) =>
              index % 10 === 0 ? (
                <RowNumber key={index} number={index}>
                  <Square
                    key={index}
                    type={getSquareType(square)}
                    squareOnClick={() =>
                      props.squareOnClick(
                        index,
                        getFiringResult(props.squares, index)
                      )
                    }
                  />
                </RowNumber>
              ) : (
                <Square
                  key={index}
                  type={getSquareType(square)}
                  squareOnClick={() =>
                    props.squareOnClick(
                      index,
                      getFiringResult(props.squares, index)
                    )
                  }
                />
              )
          )}
    </div>
  );
};

export default Grid;
