import React from "react";

const Square = props => (
  <div className={props.type} onClick={() => props.squareOnClick()} />
);

export default Square;
