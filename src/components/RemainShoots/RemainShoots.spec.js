import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import RemainShoots from "./RemainShoots";

describe("the render", () => {
  it("should render a paragraph with the text `Remaining Shoots`", () => {
    const remainShoots = shallow(<RemainShoots number={80} />);

    expect(remainShoots.find("p").text()).toEqual("Remaining Shoots");
  });
  it("should render the number of `Remaining Shoots`", () => {
    const remainShoots = shallow(<RemainShoots number={80} />);

    expect(remainShoots.find(".turns-left").text()).toEqual("80");
  });
});
