import React from "react";

const RemainShoots = props => (
  <div>
    <p>Remaining Shoots</p>
    <div className="turns-left">{props.number}</div>
  </div>
);

export default RemainShoots;
