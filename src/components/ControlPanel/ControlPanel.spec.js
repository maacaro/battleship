import React from "react";
import { shallow } from "enzyme";
import ControlPanel from "./ControlPanel";

describe("the render", () => {
  it("should render a section tag", () => {
    const controlPanelWrapper = shallow(<ControlPanel />);

    expect(controlPanelWrapper.find("section")).toHaveLength(1);
  });

  it("should render its children", () => {
    const controlPanelWrapper = shallow(
      <ControlPanel>
        <div />
        <div />
      </ControlPanel>
    );

    expect(controlPanelWrapper.children()).toHaveLength(2);
  });

  describe("the section", () => {
    it("should have the className set to `control-panel`", () => {
      const controlPanelWrapper = shallow(<ControlPanel />);
      expect(controlPanelWrapper.find("section").props().className).toEqual(
        "control-panel"
      );
    });
  });
});
