import React from "react";

const ControlPanel = props => {
  return <section className="control-panel">{props.children}</section>;
};

export default ControlPanel;
