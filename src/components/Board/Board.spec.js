import React from "react";
import { shallow } from "enzyme";
import Board from "./Board";

describe("the render", () => {
  it("should render a section tag", () => {
    const boardWrapper = shallow(<Board />);

    expect(boardWrapper.find("section")).toHaveLength(1);
  });

  it("should render its children", () => {
    const boardWrapper = shallow(
      <Board>
        <div />
        <div />
        <div />
      </Board>
    );

    expect(boardWrapper.find("section").children()).toHaveLength(3);
  });

  describe("the section tag", () => {
    it("should have the className atributte set to `board`", () => {
      const boardWrapper = shallow(<Board />);

      expect(boardWrapper.find("section").props().className).toEqual("board");
    });
  });
});
