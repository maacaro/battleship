import React from "react";

const Board = props => {
  return <section className="board">{props.children}</section>;
};

export default Board;
