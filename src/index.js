import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

const  ships = [
    { id:'s1', spaceLong: 1 },
    { id:'s2', spaceLong: 1 },
    { id:'s3', spaceLong: 2 },
    { id:'s4', spaceLong: 2 },
    { id:'s5', spaceLong: 3 },
    { id:'s6', spaceLong: 3 },
    { id:'s7', spaceLong: 3 },
    { id:'s8', spaceLong: 4 },
    { id:'s9', spaceLong: 4 },
    { id:'s10', spaceLong: 4 }];

ReactDOM.render(<App ships = {ships}/>, document.getElementById('root'));
