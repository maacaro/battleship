import React from "react";
import ReactDOM from "react-dom";
import { shallow } from "enzyme";
import App from "./App";

window.localStorage = {
  getItem: jest.fn(() => JSON.stringify([{}])),
  setItem: jest.fn()
};
it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe("the rendering", () => {
  it("should render `BattleShip` component", () => {
    const appWapper = shallow(<App />);

    expect(appWapper.find("BattleShip")).toHaveLength(1);
  });

  it("should render a section with the `className` atributte set to `control-panel`", () => {
    const appWapper = shallow(<App />);

    expect(appWapper.find("ControlPanel")).toHaveLength(1);
  });

  it("should render a section with the `className` atributte set to `board`", () => {
    const appWapper = shallow(<App />);

    expect(appWapper.find("Board")).toHaveLength(1);
  });

  describe("the `ControlPanel` component", () => {
    it("should contains a LevelGame Component", () => {
      const appWapper = shallow(<App />);

      expect(appWapper.find("ControlPanel").find("LevelGame")).toHaveLength(1);
    });

    it("should render the LevelGame Component with the `disabled` props set to false", () => {
      const appWrapper = shallow(<App />);

      expect(
        appWrapper
          .find("ControlPanel")
          .find("LevelGame")
          .props().disabled
      ).toEqual(false);
    });

    it("should contains 1 `Star to Play` button element", () => {
      const appWapper = shallow(<App />);

      expect(appWapper.find("ControlPanel").find("button")).toHaveLength(1);
      expect(
        appWapper
          .find("ControlPanel")
          .find("button")
          .at(0)
          .text()
      ).toEqual("Start to Play");
    });

    it("should have a `Star to Play` button element disabled", () => {
      const appWapper = shallow(<App />);

      expect(
        appWapper
          .find("ControlPanel")
          .find("button")
          .at(0)
          .props().disabled
      ).toBe(true);
    });

    it("should render the remaining shoots or atttemps", () => {
      const appWrapper = shallow(<App />);
      const numberOfTurns = 10;

      appWrapper.instance().onSelectLevelGame(numberOfTurns);
      appWrapper.update();
      appWrapper
        .find("Board")
        .find("Grid")
        .props()
        .squareOnClick(5, "HIT");
      appWrapper.update();

      expect(
        appWrapper
          .find("ControlPanel")
          .find("RemainShoots")
          .props().number
      ).toEqual(9);
    });

    describe("the interaction with the `Star to Play` button", () => {
      it("should call the method `startGame` on click", () => {
        const appWrapper = shallow(<App />);

        const appInstance = appWrapper.instance();
        appInstance.startGame = jest.fn();
        appWrapper
          .find("ControlPanel")
          .find("button")
          .at(0)
          .props()
          .onClick({});

        expect(appInstance.startGame).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe("the board section", () => {
    it("should contains a `Grid` component", () => {
      const appWapper = shallow(<App />);

      expect(appWapper.find("Board").find("Grid")).toBeDefined();
    });

    describe("the `Grid`", () => {
      it("should have the `squares` prop set to an Array", () => {
        const appWapper = shallow(<App />);

        expect(
          appWapper
            .find("Board")
            .find("Grid")
            .props().squares
        ).toEqual(expect.any(Array));
      });

      it("should have a length of 100 in the `squares` prop", () => {
        const appWapper = shallow(<App />);

        expect(
          appWapper
            .find("Board")
            .find("Grid")
            .props().squares
        ).toHaveLength(100);
      });

      it("should have the `squareOnclick` prop set to a function", () => {
        const appWapper = shallow(<App />);

        expect(
          appWapper
            .find("Board")
            .find("Grid")
            .props().squareOnClick
        ).toEqual(expect.any(Function));
      });

      it("should have the `disabled` prop set to true", () => {
        const appWapper = shallow(<App />);

        expect(
          appWapper
            .find("Board")
            .find("Grid")
            .props().disabled
        ).toEqual(true);
      });
    });

    describe("the `Grid` iteraction", () => {
      it("should call the `shoot` method when a squareOnClick happens", () => {
        const appWapper = shallow(<App />);
        appWapper.instance().shoot = jest.fn();

        appWapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick();

        expect(appWapper.instance().shoot).toHaveBeenCalledTimes(1);
      });
    });
  });

  it("should contains a `GameResultModal` component", () => {
    const appWrapper = shallow(<App />);

    expect(appWrapper.find("GameResultModal")).toHaveLength(1);
  });

  describe("the `GameResultModal`", () => {
    it("should have the `isOpen` prop set to false", () => {
      const appWrapper = shallow(<App />);

      const isOpen = appWrapper.find("GameResultModal").props().isOpen;

      expect(isOpen).toEqual(false);
    });

    it("should have the `onOkButtonClick` set to a function", () => {
      const appWrapper = shallow(<App />);

      expect(
        appWrapper.find("GameResultModal").props().onOkButtonClick
      ).toEqual(expect.any(Function));
    });
  });

  describe("the GameResultModal iterraction", () => {
    it("should call the `stopGame` at the `onOkButtonClick`", () => {
      const appWrapper = shallow(<App />);
      appWrapper.instance().stopGame = jest.fn();
      appWrapper
        .find("GameResultModal")
        .props()
        .onOkButtonClick();

      expect(appWrapper.instance().stopGame).toHaveBeenCalledTimes(1);
    });
  });

  it("should contains a `ListOfGames` component", () => {
    const appWrapper = shallow(<App />);

    expect(appWrapper.find("ListOfGames")).toHaveLength(1);
  });

  describe("the `ListOfGames`", () => {
    it("sohuld have the games prop set to a Array", () => {
      const appWrapper = shallow(<App />);

      expect(appWrapper.find("ListOfGames").props().games).toEqual(
        expect.any(Array)
      );
    });
  });
});

describe("the behavior", () => {
  describe("when a Level Game is selected", () => {
    it("should enabled the `Star to Play` button", () => {
      const appWrapper = shallow(<App />);
      const numberOfTurns = 50;

      appWrapper
        .find("LevelGame")
        .props()
        .onSelectLevelGame(numberOfTurns);
      appWrapper.update();

      expect(
        appWrapper
          .find("ControlPanel")
          .find("button")
          .at(0)
          .props().disabled
      ).toBe(false);
    });

    it("should display the number of remainig shoots", () => {
      const appWrapper = shallow(<App />);
      const numberOfTurns = 50;

      appWrapper
        .find("LevelGame")
        .props()
        .onSelectLevelGame(numberOfTurns);
      appWrapper.update();

      expect(appWrapper.find("RemainShoots").props().number).toBe(50);
    });
  });

  describe("when the Game is Started", () => {
    it("should disable the LevelGame component", () => {
      const appWrapper = shallow(<App ships={[]} />);
      const appInstance = appWrapper.instance();

      appWrapper
        .find("ControlPanel")
        .find("button")
        .props()
        .onClick({});
      appWrapper.update();

      expect(appWrapper.find("LevelGame").props().disabled).toEqual(true);
    });

    it("should position the ships on the board", () => {
      const appWrapper = shallow(<App ships={[]} />);
      const appInstance = appWrapper.instance();
      appInstance.positionShipOnBoard = jest.fn();

      appWrapper
        .find("ControlPanel")
        .find("button")
        .props()
        .onClick({});
      appWrapper.update();

      expect(appInstance.positionShipOnBoard).toHaveBeenCalledTimes(1);
    });

    it("should enabled the `Grid` component", () => {
      const appWrapper = shallow(<App ships={[]} />);
      const appInstance = appWrapper.instance();

      appWrapper
        .find("ControlPanel")
        .find("button")
        .props()
        .onClick({});
      appWrapper.update();

      expect(appWrapper.find("Grid").props().disabled).toEqual(false);
    });
  });

  describe("when there is shoot", () => {
    describe("when the shoot is a `HIT`", () => {
      it("should update the hited square item with `HIT`", () => {
        const appWrapper = shallow(<App />);
        const appInstance = appWrapper.instance();

        appWrapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick(5, "HIT");
        appWrapper.update();

        expect(appWrapper.find("Grid").props().squares[5]).toEqual("HIT");
      });

      it("should update the hited square item with `HIT`, when the returned value is `SINK", () => {
        const appWrapper = shallow(<App />);
        const appInstance = appWrapper.instance();

        appWrapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick(5, "SINK");
        appWrapper.update();

        expect(appWrapper.find("Grid").props().squares[5]).toEqual("HIT");
      });

      it("should NOT update the hited square item with `HIT`, when the returned value is `SINK", () => {
        const appWrapper = shallow(<App />);
        const appInstance = appWrapper.instance();
        const prevValue = appWrapper.find("Grid").props().squares[5];

        appWrapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick(5, "REPETED_SHOT");
        appWrapper.update();

        expect(appWrapper.find("Grid").props().squares[5]).toEqual(prevValue);
      });
    });

    describe("when the shoot is a `MISS`", () => {
      it("should update the missed square item with `MISS`", () => {
        const appWrapper = shallow(<App />);
        const appInstance = appWrapper.instance();

        appWrapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick(7, "MISS");
        appWrapper.update();

        expect(appWrapper.find("Grid").props().squares[7]).toEqual("MISS");
      });

      it("should NOT update the missed square item with `HIT`, when the returned value is `miss", () => {
        const appWrapper = shallow(<App />);
        const appInstance = appWrapper.instance();
        const prevValue = appWrapper.find("Grid").props().squares[5];

        appWrapper
          .find("Board")
          .find("Grid")
          .props()
          .squareOnClick(5, "REPETED_SHOT");
        appWrapper.update();

        expect(appWrapper.find("Grid").props().squares[5]).toEqual(prevValue);
      });
    });
  });

  describe("when the Game End", () => {
    it("should Save the Game Data", () => {
      const appWrapper = shallow(<App />);
      const appInstance = appWrapper.instance();

      appWrapper.instance().stopGame();

      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
    });
  });
});

describe("positionShipOnBoard", () => {
  it("should position all the ship on the board without overlapping", () => {
    const ships = [
      { id: "s4", spaceLong: 1 },
      { id: "s3", spaceLong: 2 },
      { id: "s2", spaceLong: 3 },
      { id: "s1", spaceLong: 4 }
    ];

    const appWrapper = shallow(<App ships={ships} />);
    appWrapper
      .find("ControlPanel")
      .find("button")
      .props()
      .onClick({});
    appWrapper.update();

    expect.assertions(4);
    expect(
      appWrapper
        .find("Grid")
        .props()
        .squares.filter(square => square === "s4")
    ).toEqual(["s4"]);
    expect(
      appWrapper
        .find("Grid")
        .props()
        .squares.filter(square => square === "s3")
    ).toEqual(["s3", "s3"]);
    expect(
      appWrapper
        .find("Grid")
        .props()
        .squares.filter(square => square === "s2")
    ).toEqual(["s2", "s2", "s2"]);
    expect(
      appWrapper
        .find("Grid")
        .props()
        .squares.filter(square => square === "s1")
    ).toEqual(["s1", "s1", "s1", "s1"]);
  });

  it("should position all the ship inside the board", () => {
    const ships = [
      { id: "s4", spaceLong: 1 },
      { id: "s3", spaceLong: 2 },
      { id: "s2", spaceLong: 3 },
      { id: "s1", spaceLong: 4 }
    ];
    const appWapper = shallow(<App ships={ships} />);
    appWapper.instance().positionShipOnBoard();
    appWapper.update();

    expect.assertions(4);
    const squares = appWapper.find("Grid").props().squares;
    expect(
      squares.indexOf("s4") % 10 === squares.lastIndexOf("s4") % 10 ||
        squares.indexOf("s4") % 10 < squares.lastIndexOf("s4") % 10
    ).toEqual(true);
    expect(
      squares.indexOf("s3") % 10 === squares.lastIndexOf("s3") % 10 ||
        squares.indexOf("s3") % 10 < squares.lastIndexOf("s3") % 10
    ).toEqual(true);
    expect(
      squares.indexOf("s2") % 10 === squares.lastIndexOf("s2") % 10 ||
        squares.indexOf("s2") % 10 < squares.lastIndexOf("s2") % 10
    ).toEqual(true);
    expect(
      squares.indexOf("s1") % 10 === squares.lastIndexOf("s1") % 10 ||
        squares.indexOf("s1") % 10 < squares.lastIndexOf("s1") % 10
    ).toEqual(true);
  });
});

describe("Wining", () => {
  it("should render the `GameResultModal` with the isOpen prop set to true`", () => {
    const appWrapper = shallow(<App />);
    const appInstance = appWrapper.instance();
    appWrapper.setState({ enemyShips: 1 });
    appWrapper.update();

    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper.update();

    const isOpen = appWrapper.find("GameResultModal").props().isOpen;

    expect(isOpen).toEqual(true);
  });

  it("should pop up a Modal windows with the test `You Win!!`", () => {
    const appWrapper = shallow(<App />);
    const appInstance = appWrapper.instance();

    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "SINK");
    appWrapper.update();

    const message = appWrapper.find("GameResultModal").props().message;
    appWrapper.update();

    expect(message).toEqual("You Win!!");
  });
});

describe("lossing", () => {
  it("should render the GameResultModal with the isOpen prop set to true`", () => {
    const appWrapper = shallow(<App />);
    const appInstance = appWrapper.instance();
    appWrapper.setState({ numberOfTurns: 1, numberOfAttempts: 0 });
    appWrapper.update();

    appWrapper
      .find("Board")
      .find("Grid")
      .props()
      .squareOnClick(5, "MISS");
    appWrapper.update();

    const isOpen = appWrapper.find("GameResultModal").props().isOpen;

    expect(isOpen).toEqual(true);
  });
});
