This is a web App version of the board game Battle Ship. It was made with reactJS with the create-react-app tool build.

The development process was made through "test-driven-development" TDD.
The test written was Unit Test. 

Jest has been used as a test framework and enzyme library has been used as a testing utility.

And also Prettier has been used as a code formatter.

To install the project just clone the repository and in the project folder run "npm install".

To start thr App run " npm start" in the project root folder(Battleship).
